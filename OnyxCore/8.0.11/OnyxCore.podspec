Pod::Spec.new do |s|
  s.name             = "OnyxCore"
  s.version          = "8.0.11"
  s.summary          = "Core C++ library for the ONYX SDK"
  s.homepage         = "https://www.telosid.com"
  s.license          = { :type => 'custom', :text => 'Copyright 2021 Telos ID, Inc. Refer to your executed licensing agreement with Telos ID, Inc.' }
  s.author           = { "Will Lucas" => "william.lucas@telos.com" }
  s.source           = { :http => "https://gitlab.com/api/v4/projects/29462567/packages/generic/onyx-core-framework/#{s.version}/onyx-core.framework.zip" }

  s.platform     = :ios, '9.0'
  s.requires_arc = false

  s.preserve_paths = 'onyx-core.framework'
  s.source_files = 'onyx-core.framework/Versions/A/Headers/**/*.h'
  s.vendored_frameworks = 'onyx-core.framework'

  s.frameworks = 'UIKit', 'CoreText'
  s.dependency 'OpenCV', '4.5.3'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
