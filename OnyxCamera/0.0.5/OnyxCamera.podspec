#
# Be sure to run `pod lib lint OnyxCamera.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name                    = 'OnyxCamera'
  s.version                 = '0.0.5'
  s.summary                 = 'Onyx SDK CocoaPod for iOS.'
  s.description             = 'Onyx software development kit for iOS distributed via CocoaPods'
  s.homepage                = 'https://gitlab.com/telosid/onyx/onyx-kit-ios'
  s.author                  = { 'mjwheatley' => 'mwheatley@diamondfortress.com', 'cwheatley' => 'cwheatley@diamondfortress.com' }
  s.source                  = { :http => "https://gitlab.com/api/v4/projects/29462567/packages/generic/onyx-camera-framework/#{s.version}/OnyxCamera.framework.zip" }
  s.license                 = { :type => 'custom', :text => 'Copyright 2021 Telos ID, Inc. Refer to your executed licensing agreement with Telos ID, Inc.' }
  s.ios.deployment_target   = '11.0'
  s.requires_arc            = true
  s.frameworks              = 'CoreMedia', 'AVFoundation', 'AssetsLibrary'
  s.dependency                'OnyxCore', '7.2.0'
  s.dependency                'TensorFlowLiteObjC'

  s.resource_bundles = {
    'Resources' => ['OnyxCamera.framework/Resources/*.tflite', 'OnyxCamera.framework/Resources/Assets.car']
  }

  s.preserve_paths = 'OnyxCamera.framework'
  s.source_files = 'OnyxCamera.framework/Versions/A/Headers/**/*.h'
  s.vendored_frameworks = 'OnyxCamera.framework'

  s.pod_target_xcconfig     = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
